-- Utility functions
function grid_to_pos(x, y)
	return tile_size*(x+0.5)+grid_offset.x, tile_size*(y+0.5)+grid_offset.y
end