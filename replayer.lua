local class = require("middleclass")
local json = require("json")
local inspect = require("inspect")
local utf8 = require("utf8")
local suit = require("SUIT")

local replayer = class("replayer")

suit.theme.color = {
    normal  = {bg = {50, 153, 187}, fg = {255, 255, 255}},
    active = {bg = {50, 153, 250}, fg = {255, 255, 255}},
    hovered  = {bg = {255, 153,  0}, fg = {225, 225, 225}}
}

-- Utility functions
local calc_player_rect = function(px, py)
	return {px-tile_size/4, py-tile_size/4, tile_size/2, tile_size/2}
end

function replayer:initialize(file)
	-- Open json demo file
	local f = io.open(file, "r")
	local content = f:read("*all")
	f:close()
	
	-- Load all commands
	self.commands = json.decode(content)
	
	-- Check version
	if self.commands[1].Command == "FILE_VERSION" then
		if self.commands[1].Version ~= file_version then
			print("File version is not compatible.")
			os.exit(1)
		end
	else
		print("File does not have version info.")
		os.Exit(1)
	end
	
	self.isPaused = false
	self.board_width = self.commands[2].BoardWidth
	self.board_height = self.commands[2].BoardHeight
	self.max_moves = self.commands[2].MaxMoves
	self.max_fires = self.commands[2].MaxFires
	self.max_health = self.commands[2].MaxHealth
	self.game_count = 1
	self.turn_count = 1
	self.winner_id = 0
	self.current_player_id = 0
	self.cmd_index = 3
	self.cmd_wait = 0.1
	self.players = {}
	self.fire_points = {}
	self.grid_lines = {}
	self.coord_pos = {}
	self.game_indexes = {}
	self.turn_indexes = {}
	self.info_pos = {(self.board_width+2.5)*tile_size-isv, grid_offset.y}
	self.timer = 0
	self.player_count = 0
	self.game_info_str = {}
	self.game_change = true
	self.pbutton = utf8.char(0xf04b)
	
	-- Calc grid points
	local gx, gy = grid_offset.x, grid_offset.y
	for i=0,self.board_width do
		table.insert(self.grid_lines, {i*tile_size+gx, gy, i*tile_size+gx, self.board_height*tile_size+gy})
	end
	for i=0,self.board_height do
		table.insert(self.grid_lines, {gx, i*tile_size+gy, self.board_width*tile_size+gx, i*tile_size+gy})
	end
	
	-- Calc coordinate positions
	for i=0,self.board_width-1 do
		local ww = font_16:getWidth(tostring(i))
		local xx = i * tile_size + gx + (tile_size - ww) / 2
		local yy = gy - tile_size / 2
		table.insert(self.coord_pos, {xx, yy})
	end
	for i=0,self.board_height-1 do
		local hh = font_16:getHeight()
		local xx = gx - tile_size / 2
		local yy = i * tile_size + gy + (tile_size - hh) / 2
		table.insert(self.coord_pos, {xx, yy})
	end
	
	-- Find game indexes and turn indexes
	for i, v in pairs(self.commands) do
		if v.Command == "NEW_GAME" then
			table.insert(self.game_indexes, {i, {}})
		elseif v.Command == "NEW_TURN" then
			table.insert(self.game_indexes[#self.game_indexes][2], i)
		end
	end
end

function replayer:add_player(name, id, x, y, maj, min)
	self.player_count = self.player_count + 1
	local px, py = grid_to_pos(x, y)
	self.players[id] = {
		["name"] = name,
		["id"] = id,
		["x"] = x,
		["y"] = y,
		["version"] = tostring(maj) .. "." .. tostring(min),
		["color"] = player_colors[self.player_count][1],
		["health"] = self.max_health,
		["moves_left"] = self.max_moves,
		["fires_left"] = self.max_fires,
		["isDead"] = false,
		["isRemoved"] = false,
		["px"] = px,
		["py"] = py,
		["rect"] = calc_player_rect(px, py),
		["info_pos"] = {(self.board_width+2.5)*tile_size, grid_offset.y+self.player_count*info_card_space},
		["info_str"] = {},
		["icon_str"] = {}
	}
end

function replayer:reset_player(id, x, y)
	local px, py = grid_to_pos(x, y)
	self.players[id].x = x
	self.players[id].y = y
	self.players[id].px = px
	self.players[id].py = py
	self.players[id].rect = calc_player_rect(px, py)
	self.players[id].health = self.max_health
	self.players[id].moves_left = self.max_moves
	self.players[id].fires_left = self.max_fires
	self.players[id].isDead = false
end

function replayer:new_game(count)
	self.game_count = count
	self.winner_id = 0
	self.turn_count = 1
	self.current_player_id = 0
	self.fire_points = {}
	self.turn_indexes = self.game_indexes[count][2]
end

function replayer:new_turn(count, id)
	self.turn_count = count
	self.current_player_id = id
	self.players[id].moves_left = self.max_moves
	self.players[id].fires_left = self.max_fires
end

function replayer:winner(id)
	self.winner_id = id
end

function replayer:remove_player(id)
	self.players[id].isRemoved = true
end

function replayer:kill_player(id)
	self.players[id].isDead = true
end

function replayer:damage_player(id)
	self.players[id].health = self.players[id].health - 1
end

function replayer:move(id, dir)
	local p = self.players[id]
	if dir == 0 then p.x,p.y=p.x-1,p.y-1
	elseif dir == 1 then p.y=p.y-1
	elseif dir == 2 then p.x,p.y=p.x+1,p.y-1
	elseif dir == 3 then p.x=p.x+1
	elseif dir == 4 then p.x,p.y=p.x+1,p.y+1
	elseif dir == 5 then p.y=p.y+1
	elseif dir == 6 then p.x,p.y=p.x-1,p.y+1
	elseif dir == 7 then p.x=p.x-1
	end
	p.px, p.py = grid_to_pos(p.x, p.y)
	p.rect = calc_player_rect(p.px, p.py)
	p.moves_left = p.moves_left - 1
	self.players[id] = p
end

function replayer:fire(id, dir)
	local p = self.players[id]
	local ex = p.x
	local ey = p.y

	-- Start point
	table.insert(self.fire_points, p.px)
	table.insert(self.fire_points, p.py)
	
	if dir == 0 then
		while ex > 0 and ey > 0 do
			ex = ex - 1
			ey = ey - 1
		end
		ex = ex - 0.5
		ey = ey - 0.5
	elseif dir == 1 then
		while ey > 0 do
			ey = ey - 1
		end
		ey = ey - 0.5
	elseif dir == 2 then
		while ex < self.board_width-1 and ey > 0 do
			ex = ex + 1
			ey = ey - 1
		end
		ex = ex + 0.5
		ey = ey - 0.5
	elseif dir == 3 then
		while ex < self.board_width-1 do
			ex = ex + 1
		end
		ex = ex + 0.5
	elseif dir == 4 then
		while ex < self.board_width-1 and ey < self.board_height-1 do
			ex = ex + 1
			ey = ey + 1
		end
		ex = ex + 0.5
		ey = ey + 0.5
	elseif dir == 5 then
		while ey < self.board_height-1 do
			ey = ey + 1
		end
		ey = ey + 0.5
	elseif dir == 6 then
		while ex > 0 and ey < self.board_height-1 do
			ex = ex - 1
			ey = ey + 1
		end
		ex = ex - 0.5
		ey = ey + 0.5
	elseif dir == 7 then
		while ex > 0 do
			ex = ex - 1
		end
		ex = ex - 0.5
	end
	
	local eex, eey = grid_to_pos(ex, ey)
	
	-- End point
	table.insert(self.fire_points, eex)
	table.insert(self.fire_points, eey)
	
	-- If same tile, don't draw fire
	if p.x == ex and  p.y == ey then fire_points = {} end
	
	self.players[id].fires_left = self.players[id].fires_left - 1
end

function replayer:update(dt)
	self.timer = self.timer + dt
	
	love.graphics.setFont(icons)
	
	suit.layout:reset(self.info_pos[1], self.info_pos[2]+tsh*3.5, 2, 0)
	
	-- Previous game
	if suit.Button(utf8.char(0xf048), suit.layout:col(32,32)).hit then
		local total_games = #self.game_indexes
		local previous_game = self.game_count-1
		if previous_game < 1 then
			previous_game = total_games
		end
		self.cmd_index = self.game_indexes[previous_game][1]
		self.game_change = true
		self.isPaused = false
		self.pbutton = utf8.char(0xf04b)
		self.cmd_wait = 0.001
	end
	
	-- Slower
	if suit.Button(utf8.char(0xf04a), suit.layout:col(32,32)).hit then
		self.cmd_wait = self.cmd_wait * 2
		if self.cmd_wait > 0.4 then self.cmd_wait = 0.4 end
	end
	
	-- Play
	if suit.Button(self.pbutton, suit.layout:col(32,32)).hit then
		if self.isPaused then
			self.isPaused = false
			self.pbutton = utf8.char(0xf04c)
			else
			self.isPaused = true
			self.pbutton = utf8.char(0xf04b)
		end
	end
	
	-- Stop
	if suit.Button(utf8.char(0xf04d), suit.layout:col(32,32)).hit then
		self.cmd_index = self.game_indexes[1][1]
		self.game_change = true
		self.isPaused = false
		self.pbutton = utf8.char(0xf04b)
		self.cmd_wait = 0.001
	end
	
	-- Faster
	if suit.Button(utf8.char(0xf04e), suit.layout:col(32,32)).hit then
		self.cmd_wait = self.cmd_wait * 0.5
		if self.cmd_wait < 0.025 then self.cmd_wait = 0.025 end
	end
	
	-- Next game
	if suit.Button(utf8.char(0xf051), suit.layout:col(32,32)).hit then
		local total_games = #self.game_indexes
		local next_game = self.game_count+1
		if next_game > total_games then
			next_game = 1
		end
		self.cmd_index = self.game_indexes[next_game][1]
		self.game_change = true
		self.isPaused = false
		self.pbutton = utf8.char(0xf04b)
		self.cmd_wait = 0.001
	end
	
	love.graphics.setFont(font_16)
	
	local speed = 0.1 / self.cmd_wait
	if speed > 4 then
		speed = 1
	end
	speed = tostring(speed) .. "x"
	
 	-- Info strings
	self.game_info_str = {
		bold_text_color, "Current game\t", text_color, self.game_count .. "/" .. #self.game_indexes .. "\n\n",
		bold_text_color, "Current turn\t", text_color, self.turn_count .. "/" .. #self.game_indexes[self.game_count][2] .. "\n\n",
		bold_text_color, "Replay speed\t", text_color, speed
	}
	
	for _, v in pairs(self.players) do
		local n, h, m, f
		if v.isDead then
			n = "Dead\n\n"
			h = "-\n\n"
			m = "-\n\n"
			f = "-\n\n"
		else
			n = v.name .. " " .. v.version .. "\n\n"
			h = v.health .. "/" .. self.max_health .. "\n\n"
			m = v.moves_left .. "/" .. self.max_moves .. "\n\n"
			f = v.fires_left .. "/" .. self.max_fires .. "\n\n"
		end
		v.info_str = {
			bold_text_color, "Player [" .. v.id .. "]\t", text_color, n,
			bold_text_color, "Health\t\t", text_color, h,
			bold_text_color, "Moves left\t", text_color, m,
			bold_text_color, "Fires left\t", text_color, f
		}
		
		v.icon_str = {
			bold_text_color, utf8.char(61444) .. "\n\n" .. utf8.char(62109) .. "\n\n" .. utf8.char(61549)
		}
	end
		
	-- Process commands every cmd_time seconds
	if self.timer >= self.cmd_wait and (not self.isPaused) then
		self.timer = 0
		self.fire_points = {}
		local cmd = self.commands[self.cmd_index]
		if self.game_change and cmd.Command ~= "NEW_GAME" and cmd.Command ~= "RESET_PLAYER" and cmd.Command ~= "ADD_PLAYER" then
			self.game_change = false
			self.isPaused = true
			self.cmd_wait = 0.1
		end
		if cmd.Command == "ADD_PLAYER" then
			self:add_player(cmd.Name, cmd.ID, cmd.X, cmd.Y, cmd.Major, cmd.Minor)
		elseif cmd.Command == "RESET_PLAYER" then
			self:reset_player(cmd.ID, cmd.X, cmd.Y)
		elseif cmd.Command == "REMOVE_PLAYER" then
			self:remove_player(cmd.ID)
		elseif cmd.Command == "NEW_GAME" then
			self:new_game(cmd.Count)
		elseif cmd.Command == "NEW_TURN" then
			self:new_turn(cmd.Count, cmd.ID)
		elseif cmd.Command == "MOVE" then
			self:move(cmd.ID, cmd.Direction)
		elseif cmd.Command == "FIRE" then
			self:fire(cmd.ID, cmd.Direction)
		elseif cmd.Command == "DAMAGE_PLAYER" then
			self:damage_player(cmd.ID)
		elseif cmd.Command == "KILL_PLAYER" then
			self:kill_player(cmd.ID)
		elseif cmd.Command == "WINNER" then
			self:winner(cmd.ID)
		end
		self.cmd_index = self.cmd_index + 1
	end
	
	if self.cmd_index == #self.commands then
		return false
	else
		return true
	end
end

function replayer:draw()
	-- Draw board
	love.graphics.setColor(grid_color)
	for _, v in pairs(self.grid_lines) do
		love.graphics.line(v)
	end
	
	-- Draw coordinates
	love.graphics.setColor(text_color)
	for i, v in pairs(self.coord_pos) do
		local n = i
		if i > self.board_width then
			n = i - self.board_width
		end
		love.graphics.print(tostring(n-1), v[1], v[2])
	end
	
	-- Draw fire
	love.graphics.setColor(fire_color)
	if #self.fire_points > 0 then
		love.graphics.line(self.fire_points)
	end
	
	-- Draw game info
	local px, py = unpack(self.info_pos)
	love.graphics.setColor(reset_color)
	love.graphics.print(self.game_info_str, px, py, 0, 1.25)
	
	-- Draw controls
	suit.draw()
	
	-- Draw players
	for _, v in pairs(self.players) do
		if not v.isRemoved then
			if v.isDead then
				love.graphics.setColor(255, 255, 255, 50)
			else
				love.graphics.setColor(v.color)
			end
			love.graphics.rectangle("fill", unpack(v.rect))
			love.graphics.print(v.name, v.rect[1], v.rect[2]-tile_size/2)
		end
		
		-- Draw info
		local px, py = unpack(v.info_pos)
		love.graphics.setColor(v.color)
		love.graphics.rectangle("fill", px-isv, py+tsh, 10, 10)
		love.graphics.setColor(reset_color)
		love.graphics.setFont(icons)
		love.graphics.printf(v.icon_str, px-isv, py+tsh*2.1, 16, "right", 0, 0.7)
		love.graphics.setFont(font_16)
		love.graphics.print(v.info_str, px, py+tsh, 0, 1.25)

	end
end

return replayer
