function love.conf(t)
	t.version = "0.10.0"
	t.window.title = "Kiai Player"
	t.window.width = 300
	t.window.height = 300
	t.window.vsync = false
end
