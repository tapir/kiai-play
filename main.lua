local replayer = require("replayer")

-- Global constants
width			= 1280
height			= 720
d_width			= 300
d_height		= 300
tile_size		= 32
info_card_space	= 100
isv				= 20
tsh				= 20
tsv				= 80
grid_offset		= { ["x"] = 32, ["y"] = 60 }
file_version	= "KIA_JSON_0.9"
bg_image		= love.graphics.newImage("assets/bg.png")
drag_image		= love.graphics.newImage("assets/drag.png")
font_16			= love.graphics.newFont("assets/coders_crux.ttf", 16)
icons			= love.graphics.newFont("assets/fontawesome-webfont.ttf", 16)
reset_color		= {255, 255, 255}
grid_color		= {81, 127, 164}
fire_color		= {255, 255, 0}
clear_color		= {0, 0, 0}
text_color		= {255, 255, 255}
bold_text_color	= {255, 255, 156}
trans_box_color	= {0, 0, 0, 35}
player_colors	= {
	{{227, 60, 0},		{255, 122, 74}},
	{{23, 160, 114},	{67, 256, 162}},
	{{255, 204, 51},	{255, 237, 181}},
	{{146, 204, 244},	{51, 146, 210}},
	{{255, 255, 255},	{255, 255, 255}}
}

local current_play = nil
local current_file = ""

-- Utility functions
function grid_to_pos(x, y)
	return tile_size*(x+0.5)+grid_offset.x, tile_size*(y+0.5)+grid_offset.y
end

function love.load()
    -- Line settings
	love.graphics.setLineStyle("rough")
	love.graphics.setLineJoin("miter")
	
	-- Font settings
	font_16:setFilter("linear", "linear", 16)
	icons:setFilter("linear", "linear", 16)
	love.graphics.setFont(font_16)
end

function love.update(dt)
	if current_play ~= nil then
		local x = current_play:update(dt)
		if not x then
			current_play = nil
			love.window.setMode(d_width, d_height)
		end
	end
end

function love.draw()
	love.graphics.setColor(reset_color)
	love.graphics.draw(bg_image, 0, 0)
	if current_play == nil then
		love.graphics.draw(drag_image, (d_width-drag_image:getWidth())/2, (d_height-drag_image:getHeight())/2)
	else
		love.graphics.print({bold_text_color, "Current File ", text_color, current_file}, grid_offset.x, tile_size/2, 0, 1.25)
		current_play:draw()
	end
end

function love.filedropped(file)
	if current_play == nil then
		love.window.setMode(width, height)
	end
	current_file = file:getFilename()
	current_play = replayer(current_file)
end
